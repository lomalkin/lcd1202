#!/usr/bin/php
<?php

$cnf = array();
$cnf['port'] = '/dev/ttyACM1';
$cnf['brightness'] = 200; //0-255

$cnf['dump'] = '/home/lomalkin/Dropbox/arduino/display1202/last_session3.hex';
$cnf['dump_ard'] = '/home/lomalkin/Dropbox/arduino/display1202/arduino3.txt';
$cnf['logs_enabled'] = false;

$cnf['debug'] = false;

$cnf['init_connection'] = false;
$cnf['init_display'] = false;

$bin_buf = '';

include "php_serial.class.php";
include "fonts.inc.php";

$serial = new phpSerial;

$serial->deviceSet($cnf['port']);

$serial->confBaudRate(9600); //9600
$serial->confParity("none");
$serial->confCharacterLength(8);
$serial->confStopBits(1);
$serial->confFlowControl("none");

$serial->deviceOpen();

if ($cnf['logs_enabled']) {
	if ($cnf['dump']) {
		$cnf['dumpfile'] = fopen($cnf['dump'], "wb+");
	}
	if ($cnf['dump_ard']) {
		$cnf['dump_arduino'] = fopen($cnf['dump_ard'], "w+");
	}
}

while(1) {
	lcd_read();
	if ($cnf['init_display']) {
		//lcd_string(`/home/lomalkin/Dropbox/_lomalkin/_bin/shortik`);
		lcd_string($argv[1]);
		//lcd_test();
		/*
		for ($i=0; $i<=255; $i+=1)
			lcd_char($i);
		//*/
		
		real_send();
		die("Initilized!\n");
		/*
		for ($i=0; $i<=255; $i+=1)
			lcd_brightness($i);
		for ($i=255; $i>0; $i-=1)
			lcd_brightness($i);
		//*/
	} else {
		echo "Display initalizing...\n";
		sleep(2);
		//lcd_init();
		//now init in arduino fw
		lcd_brightness(255);
		$cnf['init_display'] = true;
	}
}

function lcd_cmd($command, $value) {
	global $serial, $cnf, $bin_buf;
	$cmd = chr($command) . chr($value);
	if ($cnf['debug']) {
		echo "OUT:\t" . dechex($command) . "\t" . dechex($value) . "\t[$command\t$value]\n";
	}
	if ($cnf['logs_enabled']) {
		if ($cnf['dump_arduino']) {
			$arduino_code = "lcd(0x" . dechex($command) . ", 0x" . dechex($value) . ");\n";
			fwrite($cnf['dump_arduino'], $arduino_code);
		}
		if ($cnf['dumpfile']) {
			fwrite($cnf['dumpfile'], chr($command) . chr($value));
		}
	} //logs
	$bin_buf .= $cmd;
	//$serial->sendMessage($cmd); //use real_send()
	//lcd_read();
} //

function real_send() {
	global $serial, $bin_buf;
	$serial->sendMessage($bin_buf);
} //

function lcd($arr, $val = 1) {
	if (is_array($arr)) {
		foreach ($arr as $a) {
			lcd_cmd($a, $val);
		}
	} else {
		lcd_cmd($arr, $val);
	}
} //

function lcd_brightness($br) {
	lcd_cmd(0xE3, $br);
} //

function lcd_init() {
	global $cnf;
	lcd_brightness($cnf['brightness']);
	
	//http://tuxotronic.org/wiki/component/lcd/ste2007
	lcd_cmd(0xE2, 0); // Reset
	lcd_cmd(0xA4, 0); // Power saver off
	lcd_cmd(0x2F, 0); // Power control set

	lcd_cmd(0xC8, 0); //MIRROR_VERTICAL_OFF = 0xC7, MIRROR_VERTICAL_ON = 0xC8,
	lcd_cmd(0xA1, 0); //MIRROR_HORIZONTAL_OFF = 0xA0, MIRROR_HORIZONTAL_ON = 0xA1,
	
	lcd_cmd(0x00, 0);
	lcd_cmd(0x10, 0);
	
	lcd_cmd(0x2F, 0); //CHARGE_PUMP_ON
	lcd_cmd(0x90, 0); //contrast normal
	
	lcd_cmd(0xAF, 0); // LCD display on
	 
	lcd_cmd(0x00, 0); // Page address set (0xB0 | 0)
	
	lcd_clear();
	
	for ($i=0; $i<96; $i++) {
		lcd(255,255);
	}
	$cnf['init_display'] = true;
}

function lcd_clear() {
	//lcd_cmd(0xAE, 0); //OFF
	lcd_cmd(0xB0, 0); //Y
	lcd_cmd(0x10, 0); //X UPPER
	lcd_cmd(0x00, 0); //X LOWER
	//*
	for ($i=0; $i<864; $i++) {
		lcd_cmd(0x00, 1); //clear all
	} //*/
	lcd_cmd(0xAF, 0); //ON
} //very slooooow

function lcd_string($str) {
	$str = iconv('utf-8', 'cp1251', $str);
	for ($i=0; $i<strlen($str); $i++) {
		$chr = substr($str, $i, 1);
		lcd_char($chr);
	}
} //

function lcd_char($chr) {
	global $cnf;
	if (is_string($chr)) {
		$chr = ord($chr);
	}
	if (($chr>=32)&&($chr<=126)){
		$chr -= 32;
	} else if (($chr>=192)&&($chr<=255)){
		$chr -= 97;
	}else{
		echo "ERROR: Invalid character $chr, [".chr($chr)."].\n";
	}
	//echo "DEBUG: chr=$chr [" . chr($chr) . "]\n";
	lcd(array_slice($cnf['font1'], $chr*5, 5));
	lcd(0x00, 1); //space
}

function lcd_test() {
	$str = '"Mama mila ramu \n"';
	$str .= 'Мама Мыла раму!!!';
	$str .= ' Holodilnik.';
	lcd_string($str);
}

function lcd_read() {
	global $serial, $cnf;
	
	$answ = $serial->readPort();
	if ($answ) {
		$str = trim($answ);
		echo "> $str\n";
		if ($str == "Init OK.") {
			$cnf['init_connection'] = true;
		}
	}
} //
