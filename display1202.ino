#include <SPI.h>

/*
SCLK — Serial Clock: тактовый сигнал (от ведущего)
  Другие обозначения: SCK, CLK
  Arduino: пин 13
MOSI — Master Output, Slave Input: данные от ведущего к ведомому
  Другие обозначения: SDI, DI, SI
  Arduino: пин 11
MISO — Master Input, Slave Output: данные от ведомого к ведущему
  Другие обозначения: SDO, DO, SO
  Arduino: пин 12
SS — Slave Select: выбор ведомого; устанавливается ведущим
  Другие обозначения: nCS, CS, CSB, CSN, nSS, STE
  Arduino: по умолчанию пин 10
*/

const int SPI_delay = 1; //10

const int LCD_LED = 9;
int LCD_br = 64;

const int SPI_CS = 10;
const int SPI_MOSI = 11;
const int SPI_CLK = 12;    //
int SPI_bit = 0;

int command = 0;
int value = 0;
int data = 0;
int cnt = 0;
int bitnum = 0;

int i=0;

void setup() {
  Serial.begin(9600); //9600
  
  pinMode (SPI_CS, OUTPUT);
  pinMode (SPI_MOSI, OUTPUT);
  pinMode (SPI_CLK, OUTPUT);
  
  pinMode (LCD_LED, OUTPUT);
  
  analogWrite(LCD_LED, LCD_br);
  digitalWrite(SPI_CS, HIGH);
  delay(100);
  digitalWrite(SPI_CS, LOW);

  lcd_init();  
  Serial.println("Init OK.");
  
  /*
  Serial.println("START TESTING");
    lcd_test();//*/
}

void loop() {
  if (Serial.available()) {
    data = Serial.read();

    cnt = cnt + 1;
    if (cnt == 1) {
      command = data;
    }
    if (cnt == 2) {
      if (command == 0xE3) {  //NOP for display processor, but we want using it for brightness control.
        LCD_br = data;
        analogWrite(LCD_LED, LCD_br);
        Serial.print("SET Brightness: ");
        Serial.println(data);
      } else {
        if (data > 0) {
          value = 1;
        } else {
          value = 0;
        }
        /*
        Serial.print("Send: ");
        Serial.print(value);
        Serial.print(" ");
        Serial.print(command, HEX);
        Serial.println();
        //*/
        lcd(command, value);
      }
      cnt = 0;
    }
  }
}

void lcd_init() {
  lcd(0xE2, 0); // Reset
  lcd(0xA4, 0); // Power saver off
  lcd(0x2F, 0); // Power control set
  lcd(0xC8, 0); //MIRROR_VERTICAL_OFF = 0xC7, MIRROR_VERTICAL_ON = 0xC8,
  lcd(0xA1, 0); //MIRROR_HORIZONTAL_OFF = 0xA0, MIRROR_HORIZONTAL_ON = 0xA1,

  lcd(0x00, 0);
  lcd(0x10, 0);

  lcd(0x2F, 0); //CHARGE_PUMP_ON
  lcd(0x90, 0); //contrast normal

//  lcd(0xAF, 0); // LCD display on
 
  lcd(0x00, 0); // Page address set (0xB0 | 0)
  
  //clear:
  lcd(0xAE, 0); //OFF
  lcd(0xB0, 0); //Y
  lcd(0x10, 0); //X UPPER
  lcd(0x00, 0); //X LOWER
  //*
  for (i=0; i<864; i++) {
    lcd(0x00, 1); //clear all
  } //*/
  lcd(0xAF, 0); //ON
}

void lcd(int command, int value) {
  digitalWrite(SPI_CS, LOW);
 
  sendBit(value); //9th bit
  for (bitnum = 0; bitnum < 8; bitnum++) {
    if (command & 0x80)
      sendBit(1);
    else 
      sendBit(0);
    command <<= 1;
  }
  
//  digitalWrite(SPI_CS,HIGH);
  //delay(SPI_delay);
}

void sendBit(int b) {
  if (b == 1) {
    digitalWrite(SPI_MOSI, HIGH);
    //Serial.print(1);
  } else {
    digitalWrite(SPI_MOSI, LOW);
    //Serial.print(0);
  }
  delay(SPI_delay/2);
  digitalWrite(SPI_CLK, HIGH);
  delay(SPI_delay/2);
  digitalWrite(SPI_CLK, LOW);
}

void lcd_test() {
lcd(0xe3, 0xc8);
lcd(0xe2, 0x0);
lcd(0xa4, 0x0);
lcd(0x2f, 0x0);
lcd(0xc8, 0x0);
lcd(0xa1, 0x0);
lcd(0x0, 0x0);
lcd(0x10, 0x0);
lcd(0x2f, 0x0);
lcd(0x90, 0x0);
lcd(0xaf, 0x0);
lcd(0x0, 0x0);
lcd(0xb0, 0x0);
lcd(0x10, 0x0);
lcd(0x0, 0x0);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0xaf, 0x0);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0xff, 0xff);
lcd(0x1, 0x1);
lcd(0x1, 0x1);
lcd(0x7f, 0x1);
lcd(0x1, 0x1);
lcd(0x1, 0x1);
lcd(0x0, 0x1);
lcd(0x38, 0x1);
lcd(0x54, 0x1);
lcd(0x54, 0x1);
lcd(0x54, 0x1);
lcd(0x18, 0x1);
lcd(0x0, 0x1);
lcd(0x48, 0x1);
lcd(0x54, 0x1);
lcd(0x54, 0x1);
lcd(0x54, 0x1);
lcd(0x20, 0x1);
lcd(0x0, 0x1);
lcd(0x4, 0x1);
lcd(0x3f, 0x1);
lcd(0x44, 0x1);
lcd(0x40, 0x1);
lcd(0x20, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x46, 0x1);
lcd(0x49, 0x1);
lcd(0x49, 0x1);
lcd(0x49, 0x1);
lcd(0x31, 0x1);
lcd(0x0, 0x1);
lcd(0x4, 0x1);
lcd(0x3f, 0x1);
lcd(0x44, 0x1);
lcd(0x40, 0x1);
lcd(0x20, 0x1);
lcd(0x0, 0x1);
lcd(0x7c, 0x1);
lcd(0x8, 0x1);
lcd(0x4, 0x1);
lcd(0x4, 0x1);
lcd(0x8, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x44, 0x1);
lcd(0x7d, 0x1);
lcd(0x40, 0x1);
lcd(0x0, 0x1);
lcd(0x0, 0x1);
lcd(0x7c, 0x1);
lcd(0x8, 0x1);
lcd(0x4, 0x1);
lcd(0x4, 0x1);
lcd(0x78, 0x1);
lcd(0x0, 0x1);
lcd(0xc, 0x1);
lcd(0x52, 0x1);
lcd(0x52, 0x1);
lcd(0x52, 0x1);
lcd(0x3e, 0x1);
lcd(0x0, 0x1);

}
